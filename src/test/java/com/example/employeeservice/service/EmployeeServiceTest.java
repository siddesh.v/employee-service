package com.example.employeeservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.employeeservice.exception.ResourceNotFoundException;
import com.example.employeeservice.model.Employee;
import com.example.employeeservice.repository.EmployeeRepository;
import com.example.employeeservice.response.ResponseBean;

@SpringBootTest
public class EmployeeServiceTest {

	@Mock
	private EmployeeRepository employeeRepository;

	@InjectMocks
	private EmployeeService employeeService;

	private Validator validator;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
		// Create a Validator for validating @Valid annotations
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void testSaveEmployee() {
		Employee employeeToSave = new Employee();
		employeeToSave.setId(1L);
		employeeToSave.setFirstName("John");
		when(employeeRepository.save(employeeToSave)).thenReturn(employeeToSave);
		ResponseBean result = employeeService.saveEmployee(employeeToSave);
		Employee obj = (Employee) result.getData();
		assertNotNull(result);
		assertEquals(1L, obj.getId());
		assertEquals("John", obj.getFirstName());
	}

	@Test
	public void testFindEmployeeById() throws Exception {
		Employee employee = new Employee();
		employee.setId(1L);
		when(employeeRepository.findById(1L)).thenReturn(Optional.of(employee));
		Employee result = employeeService.findEmployeeById(1L);
		assertEquals(1L, result.getId());
	}

	@Test
    public void testFindEmployeeByIdNotFound() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> {
            employeeService.findEmployeeById(1L);
        });
    }

	@Test
	public void testFindByEmployeeId() throws Exception {
		Employee existingEmployee = new Employee();
		existingEmployee.setId(1L);
		existingEmployee.setFirstName("John");
		Employee updatedEmployee = new Employee();
		updatedEmployee.setId(1L);
		updatedEmployee.setFirstName("Updated John");
		when(employeeRepository.findById(1L)).thenReturn(Optional.of(existingEmployee));
		when(employeeRepository.save(existingEmployee)).thenReturn(updatedEmployee);
		Employee result = employeeService.findByEmployeeId(1L, updatedEmployee);
		assertEquals("Updated John", result.getFirstName());
	}

	@Test
    public void testFindByEmployeeIdNotFound() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());
        Employee updatedEmployee = new Employee();
        assertThrows(ResourceNotFoundException.class, () -> {
            employeeService.findByEmployeeId(1L,updatedEmployee);
        });
    }

	@Test
	public void testDeleteEmployeeById() throws Exception {
		Employee employee = new Employee();
		employee.setId(1L);
		when(employeeRepository.findById(1L)).thenReturn(Optional.of(employee));
		Map<String, Boolean> result = employeeService.deleteEmployeeById(1L);
		assertTrue(result.get("deleted"));
	}

	@Test
    public void testDeleteEmployeeByIdNotFound() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> {
            employeeService.deleteEmployeeById(1L);
        });
    }

	@Test
	public void testFindAllBySelectedSort() throws Exception {
		Employee employee1 = new Employee();
		employee1.setId(1L);
		Employee employee2 = new Employee();
		employee2.setId(2L);
		List<Employee> employeeList = new ArrayList<>();
		employeeList.add(employee1);
		employeeList.add(employee2);
		when(employeeRepository.fetchEmpAscOrder()).thenReturn(employeeList);
		ResponseBean response = new ResponseBean();
		response = (ResponseBean) employeeService.findAllBySelectedSort();
		List<Employee> result = (List<Employee>) response.getData();
		assertEquals(2, result.size());
		assertEquals(1L, result.get(0).getId());
		assertEquals(2L, result.get(1).getId());
	}
	
	@Test
	public void testFindAllByCustomSortByDesc() throws Exception{
		Employee employee1 = new Employee();
		employee1.setId(1L);
		Employee employee2 = new Employee();
		employee2.setId(2L);
		List<Employee> empList = new ArrayList<Employee>();
		empList.add(employee1);
		empList.add(employee2);
		ResponseBean responseBean = new ResponseBean();
		when(employeeRepository.fetchEmpDscOrder()).thenReturn(empList);
		responseBean = (ResponseBean) employeeService.findAllByCustomSort("d");
		List<Employee> result = (List<Employee>) responseBean.getData();
		assertEquals(2, result.size());
	}
}
