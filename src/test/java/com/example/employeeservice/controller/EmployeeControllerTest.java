package com.example.employeeservice.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;

//import static org.junit.jupiter.api.Assertions.*;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.employeeservice.model.Employee;
import com.example.employeeservice.repository.EmployeeRepository;
import com.example.employeeservice.response.ResponseBean;
import com.example.employeeservice.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest
public class EmployeeControllerTest {

	ObjectMapper mapper = new ObjectMapper();

	@MockBean
	private EmployeeService employeeService;
	@MockBean
	private EmployeeRepository employeeRepository;
	@Autowired
	private MockMvc mockMvc;

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	Employee emp_1 = new Employee(1, "John", "D", "jhon@gmail.com", 25000.00);
	Employee emp_2 = new Employee(2, "Jane", "S", "Jane@gmail", 35000.00);
	Employee emp_3 = new Employee(3, "Doe", "A", "Doe@gmail", 28000.00);

	@Test
	public void testGetAllEmployees() throws Exception {
		// Arrange
		List<Employee> expectedEmployees = Arrays.asList(emp_1, emp_2, emp_3);
		ResponseBean bean = new ResponseBean();
		Mockito.when(employeeRepository.fetchEmpAscOrder()).thenReturn(expectedEmployees);
		bean.setData(expectedEmployees);
		bean.setStatus(HttpStatus.OK);
		Mockito.when(employeeService.findAllBySelectedSort()).thenReturn(bean);
		mockMvc.perform(get("/employees/all").contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON)).andReturn();
		assertEquals(HttpStatus.OK, employeeService.findAllBySelectedSort().getStatus());

	}

	@Test
	public void testGetEmployeeById() throws Exception {
		Optional<Employee> emp = Optional
				.ofNullable(new Employee(1, "Bahubali", "Telugu", "Bahubali@gmail.com", 20000.00));
		Mockito.when(employeeRepository.findById(anyLong())).thenReturn(emp);
		Mockito.when(employeeService.findEmployeeById(anyLong())).thenReturn(emp_1);
		mockMvc.perform(get("/employees?id=1").contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
				.accept(MediaType.APPLICATION_JSON)).andReturn();
		assertEquals(1, employeeService.findEmployeeById(anyLong()).getId());
	}

	@Test
	public void testCreateEmployee() throws Exception {
		ResponseBean bean = new ResponseBean();
		Employee employee = new Employee(1, "Johnson", "Deer", "jhonD@gmail.com", 26000.00);
		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		bean.setData(employee);
		Mockito.when(employeeService.saveEmployee(employee)).thenReturn(bean);
		String json = mapper.writeValueAsString(employee);
		MvcResult requestResult = mockMvc.perform(post("/employees/save").contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf-8").content(json).accept(MediaType.APPLICATION_JSON)).andReturn();
		String result = requestResult.getResponse().getContentAsString();
		assertNotNull(result);

	}
	
	@Test
	public void testUpdateEmployee() throws Exception {
		Employee employee = new Employee(1, "Johnson", "Deer", "jhonD@gmail.com", 28000.00);
		Optional<Employee> emp = Optional
				.ofNullable(new Employee(1, "Johnson", "Deer", "jhonD@gmail.com", 26000.00));
		Mockito.when(employeeRepository.findById(any())).thenReturn(emp);
		Mockito.when(employeeService.findByEmployeeId(1L,employee)).thenReturn(employee);
		String json = mapper.writeValueAsString(employee);
		mockMvc.perform(put("/employees/update/1").contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf-8").content(json).accept(MediaType.APPLICATION_JSON)).andReturn();
		assertEquals(1,emp.get().getId());
		
	}
	
	@Test
	public void testDeleteEmployee() throws Exception{
		Employee employee = new Employee(1, "Johnson", "Deer", "jhonD@gmail.com", 28000.00);
		Optional<Employee> emp = Optional
				.ofNullable(new Employee(1, "Johnson", "Deer", "jhonD@gmail.com", 26000.00));
		Mockito.when(employeeRepository.findById(any())).thenReturn(emp);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		Mockito.when(employeeService.deleteEmployeeById(employee.getId())).thenReturn(response);
		mockMvc.perform(MockMvcRequestBuilders.delete("/employees/delete/1", employee.getId()))
        .andExpect(status().isOk());
	}
	@Test
	public void testCoustmSort() throws Exception{
		List<Employee> expectedEmployees = Arrays.asList(emp_1, emp_2, emp_3);
		ResponseBean bean = new ResponseBean();
		Mockito.when(employeeRepository.fetchEmpAscOrder()).thenReturn(expectedEmployees);
		bean.setData(expectedEmployees);
		bean.setStatus(HttpStatus.OK);
		Mockito.when(employeeService.findAllByCustomSort(anyString())).thenReturn(bean);
		mockMvc.perform(MockMvcRequestBuilders.get("/employees/customsort?sort=a"))
        .andExpect(status().isOk());

	}

}
