package com.example.employeeservice.exception;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

public class GlobalExceptionHandlerTest {

    @InjectMocks
    private GlobalExceptionHandler globalExceptionHandler;

    @Mock
    private WebRequest webRequest;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testResourceNotFoundException() {
        ResourceNotFoundException ex = new ResourceNotFoundException("Resource not found");
        ResponseEntity<?> responseEntity = globalExceptionHandler.resourceNotFoundException(ex, webRequest);

        assertNotNull(responseEntity);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof ErrorDetails);
        ErrorDetails errorDetails = (ErrorDetails) responseEntity.getBody();
        assertEquals(ex.getMessage(), errorDetails.getMessage());
        assertNotNull(errorDetails.getTimestamp());
        assertEquals(webRequest.getDescription(false), errorDetails.getDetails());
    }

    @Test
    public void testGlobalExceptionHandler() {
        Exception ex = new Exception("Internal server error");
        ResponseEntity<?> responseEntity = globalExceptionHandler.globleExcpetionHandler(ex, webRequest);

        assertNotNull(responseEntity);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof ErrorDetails);
        ErrorDetails errorDetails = (ErrorDetails) responseEntity.getBody();
        assertEquals(ex.getMessage(), errorDetails.getMessage());
        assertNotNull(errorDetails.getTimestamp());
        assertEquals(webRequest.getDescription(false), errorDetails.getDetails());
    }
}
