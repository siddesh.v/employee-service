package com.example.employeeservice.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.employeeservice.exception.ResourceNotFoundException;
import com.example.employeeservice.model.Employee;
import com.example.employeeservice.repository.EmployeeRepository;
import com.example.employeeservice.response.ResponseBean;
/*
 * 
 */
@Service
public class EmployeeService {
	  private int attempt=1;
//	private final RetryTemplate retryTemplate = new RetryTemplate();
	private final RestTemplate restTemplate = new RestTemplate();

	@Autowired
	private EmployeeRepository employeeRepository;
	private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);

	public Employee findEmployeeById(Long employeeId) throws ResourceNotFoundException {
		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
		return employee;
	}

	public ResponseBean saveEmployee(@Valid Employee employee) {
		ResponseBean responseBean = new ResponseBean();
		logger.info("Processing a request to save employee record ...", EmployeeService.class);
		Employee emp = new Employee();
		try {
			emp = employeeRepository.save(employee);
			responseBean.setData(emp);
			responseBean.setStatus(HttpStatus.CREATED);
			responseBean.setMessage("employee record saved successfully");
		} catch (Exception e) {
			responseBean.setData(emp);
			responseBean.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			responseBean.setMessage(e.getMessage());
		}

		return responseBean;
	}

	public Employee findByEmployeeId(Long employeeId, Employee employeeDetails) throws ResourceNotFoundException {
		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
		employee.setEmailId(employeeDetails.getEmailId());
		employee.setLastName(employeeDetails.getLastName());
		employee.setFirstName(employeeDetails.getFirstName());
		final Employee updatedEmployee = employeeRepository.save(employee);
		return updatedEmployee;
	}

	public Map<String, Boolean> deleteEmployeeById(Long employeeId) throws ResourceNotFoundException {
		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
		employeeRepository.delete(employee);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	public ResponseBean findAllByCustomSort(String sort) throws Exception {
		List<Employee> empList = new ArrayList<Employee>();
		ResponseBean responseBean = new ResponseBean();
		if ("a".equalsIgnoreCase(sort)) {
			empList = employeeRepository.fetchEmpAscOrder();
		} else {
			empList = employeeRepository.fetchEmpDscOrder();
		}
		if (!empList.isEmpty()) {
			responseBean.setMessage("recodrs fetched successfully");
		} else {
			responseBean.setMessage("No recodrs found");
		}
		responseBean.setData(empList);
		responseBean.setStatus(HttpStatus.OK);
		return responseBean;
	}

	public ResponseBean findAllBySelectedSort() throws Exception {
		ResponseBean bean = new ResponseBean();
		List<Employee> rsultList = employeeRepository.fetchEmpAscOrder();
		if (rsultList.isEmpty()) {
			bean.setMessage("No recodrs found ");
		} else {
			bean.setMessage("recodrs fetched successfully");
		}
		bean.setData(rsultList);
		bean.setStatus(HttpStatus.OK);
		return bean;

	}
	
	 /**
     * Default retry attempts is 3 (maxAttempts)
     * delay is in milliseconds
     **/
	
	@Retryable(value = {RuntimeException.class},
	          backoff = @Backoff(delay = 10000),
	          maxAttempts = 3)
	public String callToExternalService() {
//		retryTemplate.execute((retryContext -> {
//		logger.info("retry method called "+attempt++ +" times "+" at "+new Date());
			ResponseEntity<String> res = restTemplate.exchange("http://localhost:9292/user-service/hello", HttpMethod.GET, null, String.class);
//		}));
		return "Ok.";

	}
	/*
	 * Recover method should have same return type as the retry method
	 */
	@Recover
	public String handleBackendFailure(RuntimeException exception) {
        String errorMessage = "Please try ofter some time: ";
		return errorMessage;
       
    }
	
	

}
