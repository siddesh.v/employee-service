package com.example.employeeservice.response;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import lombok.Generated;

@Component
@Generated
public class ResponseBean {
	private String message;
	private HttpStatus status;
	private Object data;
	
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}
