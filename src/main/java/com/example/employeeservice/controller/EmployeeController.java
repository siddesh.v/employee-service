package com.example.employeeservice.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.employeeservice.exception.ResourceNotFoundException;
import com.example.employeeservice.model.Employee;
import com.example.employeeservice.response.ResponseBean;
import com.example.employeeservice.service.EmployeeService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@ApiOperation("this api is used to fetch all the records .")
	@GetMapping("/all")
	public ResponseBean getAllEmployees() throws Exception {
		logger.info("Processing a request in EmployeeController ...");
		return employeeService.findAllBySelectedSort();
	}

	@ApiOperation("this api is used to fetch all the record based on employee id .")
	@GetMapping
	public ResponseEntity<Employee> getEmployeeById(@RequestParam(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		Employee employee = employeeService.findEmployeeById(employeeId);
		return ResponseEntity.ok().body(employee);
	}
	
	@ApiOperation("this api is used to save employee record in to database .")
	@PostMapping("/save")
	public ResponseBean createEmployee(@Valid @RequestBody Employee employee) {
		return employeeService.saveEmployee(employee);
	}
	@ApiOperation("this api is used to update the employee record .")
	@PutMapping("/update/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long employeeId,
			@Valid @RequestBody Employee employeeDetails) throws ResourceNotFoundException {
		return ResponseEntity.ok(employeeService.findByEmployeeId(employeeId, employeeDetails));
	}

	@ApiOperation("this api is used to delete the employee record from data base .")
	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		return employeeService.deleteEmployeeById(employeeId);
	}

	@ApiOperation("this api is used to fetch the employees records by salary wise asc/desc order .")
	@GetMapping("/customsort")
	public ResponseBean coustmSort(@RequestParam(value = "sort") String sort) throws Exception {
		return employeeService.findAllByCustomSort(sort);
	}
	//retry poc
	
	@GetMapping("/external")
    public ResponseEntity<String> get() {
        return ResponseEntity.ok(employeeService.callToExternalService());
    }
}
