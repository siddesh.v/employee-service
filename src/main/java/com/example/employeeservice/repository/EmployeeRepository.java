package com.example.employeeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.employeeservice.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	
	@Query(value = "SELECT * FROM EMPLOYEES ORDER BY  SALARY ASC",nativeQuery = true)
	List<Employee> fetchEmpAscOrder();
	@Query(value = "SELECT * FROM EMPLOYEES ORDER BY  SALARY DESC",nativeQuery = true)
	List<Employee> fetchEmpDscOrder();
	

}
